<?php

namespace common\models;

use creocoder\nestedsets\NestedSetsBehavior;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\helpers\Html;

/**
 * This is the model class for table "menu".
 *
 * @property int $id
 * @property int $tree
 * @property int $lft
 * @property int $rgt
 * @property int $depth
 * @property string $name
 * @property string|null $seo_title
 * @property string|null $seo_keywords
 * @property string|null $seo_description
 * @property string|null $slug
 * @property int $position
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int|null $is_status
 */
class Menu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['tree', 'lft', 'rgt', 'depth',], 'safe'],
            [['is_status', 'tree'], 'default', 'value' => 0],
            [['position'], 'default', 'value' => 1],

            [['tree', 'lft', 'rgt', 'depth', 'position', 'is_status'], 'integer'],
            [['name', 'created_at', 'updated_at'], 'string', 'max' => 255],
            [['seo_title', 'seo_keywords', 'seo_description'], 'string', 'max' => 256],
            [['slug'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'tree' => Yii::t('app', 'Tree'),
            'lft' => Yii::t('app', 'Lft'),
            'rgt' => Yii::t('app', 'Rgt'),
            'depth' => Yii::t('app', 'Depth'),
            'name' => Yii::t('app', 'Name'),
            'seo_title' => Yii::t('app', 'Seo Title'),
            'seo_keywords' => Yii::t('app', 'Seo Keywords'),
            'seo_description' => Yii::t('app', 'Seo Description'),
            'slug' => Yii::t('app', 'Slug'),
            'position' => Yii::t('app', 'Position'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'is_status' => Yii::t('app', 'Is Status'),
        ];
    }


    public function behaviors()
    {
        return [
            TimestampBehavior::className(),

            [
                'class' => SluggableBehavior::class,
                'attribute' => 'name',
                'immutable' => true,
                'ensureUnique' => true,
            ],
            'tree' => [
                'class' => NestedSetsBehavior::class,
                'treeAttribute' => 'tree',
//                'leftAttribute' => 'lft',
//                'rightAttribute' => 'rgt',
//                'depthAttribute' => 'depth',
            ],
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public static function find()
    {
        return new MenuQuery(get_called_class());
    }


    /**
     * Get parent's ID
     * @return \yii\db\ActiveQuery
     */
    public function getParentId()
    {
        $parent = $this->parent;
        return $parent ? $parent->id : null;
    }

    /**
     * @return MenuQuery
     */
    public static function getParent()
    {
        return self::find()->where(['name' => 'root']);
    }


    /**
     * @return string
     *
     */
    public static function getTree(): string
    {
        $menus = self::find()->addOrderBy('lft')->all();
        $depth = 0;
        $html = '';

        foreach ($menus as $menu) {
            if ($menu->depth == $depth) {
                $html = sprintf("%s</li>\n", $html);
            } elseif ($menu->depth > $depth) {
                $html = sprintf("%s<ul>\n", $html);
            } else {
                $html = sprintf("%s</li>\n", $html);

                for ($i = $depth - $menu->depth; $i; $i--) {
                    $html = sprintf("%s</ul>\n</li>\n", $html);
                }
            }

            $html =
                sprintf(
                    "%s<li>\n <a href='menu/view/%s'>%s</a>",
                    $html,
                    $menu->slug,
                    Html::encode($menu->name)
                );

            $depth = $menu->depth;
        }

        for ($i = $depth; $i; $i--) {
            $html = sprintf("%s</li>\n</ul>\n", $html);
        }

        return $html;
    }
}
