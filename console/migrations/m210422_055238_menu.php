<?php

use yii\db\Migration;

/**
 * Class m210422_055238_menu
 */
class m210422_055238_menu extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210422_055238_menu cannot be reverted.\n";

        return false;
    }

    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%menu}}', [
            'id' => $this->primaryKey(),
            'tree' => $this->integer()->notNull(),
            'lft' => $this->integer()->notNull(),
            'rgt' => $this->integer()->notNull(),
            'depth' => $this->integer()->notNull(),
            'position'   => $this->integer()->notNull()->defaultValue(0),
            'name' => $this->string()->notNull(),
            'seo_title' => $this->string(256),
            'seo_keywords' => $this->string(256),
            'seo_description' => $this->string(256),
            'slug' => $this->string(500),
            'created_at' => $this->string(),
            'updated_at' => $this->string(),
            'is_status' => $this->tinyInteger(2)->defaultValue(0)
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%menu}}');
    }

}
