<?php

use common\models\Menu;
use slatiusa\nestable\Nestable;

/* @var $this yii\web\View */
/* @var $menu \common\models\Menu */
/* @var $customTree */

$this->title = 'My Yii Application';
?>
    <div class="site-index">
        <div class="jumbotron">
            <h1>Nested Set!</h1>
            <p class="lead">Реализовать вывод и редактирование nested set в виде 2х-уровневого меню</p>
        </div>
    </div>


    <h2> вывода Меню с помешю этой раширеной https://github.com/ASlatius/yii2-nestable</h2>
<?php
echo Nestable::widget([
    'type' => Nestable::TYPE_WITH_HANDLE,
    'query' => $menu,
    'modelOptions' => [
        'name' => 'name'
    ],
    'pluginEvents' => [
        'change' => 'function(e) {}',
    ],
    'pluginOptions' => [
        'maxDepth' => 7,
    ],
]);

?>

<h2> Custom вывода Меню <h2>

<div class="col-xs-12">
    <ul>
        <?= $customTree; ?>
    </ul>
</div>
