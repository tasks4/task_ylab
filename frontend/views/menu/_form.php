<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Menu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="menu-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-xs-12">
        <?= $form->field($model, 'is_status')
            ->widget(
                \kartik\switchinput\SwitchInput::class,
                [
                    'value' => true,
                    'pluginOptions' =>
                        [
                            'size' => 'large',
                            'onColor' => 'success',
                            'offColor' => 'danger',
                            'onText' => Yii::t('app', 'Active'),
                            'offText' => Yii::t('app', 'Inactive'),
                        ],
                ]
            );
        ?>
    </div>

    <div class="col-xs-12">
        <?= $form->field($model, 'position')->widget(
            \kartik\select2\Select2::class, [
            'data' =>
                \yii\helpers\ArrayHelper::map(
                    \common\models\Menu::find()
                        ->select('id,name')
                        ->asArray()
                        ->all(),
                    "id", "name"),
            'options' => ['placeholder' => 'Parent'],
            'pluginOptions' => ['allowClear' => true],
        ]);
        ?>

    </div>

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'seo_title')->textInput(['maxlength' => true]) ?>
            </div>

        </div>
    </div>
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-6">
                <?= $form->field($model, 'seo_keywords')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-xs-6">
                <?= $form->field($model, 'seo_description')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
